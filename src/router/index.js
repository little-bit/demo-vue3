import {createRouter, createWebHistory} from 'vue-router'

const login = () => import('../views/LoginView');
const register = () => import('../views/RegisterView');
const dashborad = () => import('../views/Dashboard')
const survey = () => import('../views/Survey')
const monitor = () => import('../views/Monitor')
const info = () => import('../views/Information')
const test = () => import('../views/test')
const routes = [


  {
    path: '/',
    redirect: login,
    component: login
  },
  {
    path: '/login',
    name: 'login',
    component: login
  },
  {
    path: '/test',
    name: 'test',
    component: test
  },
  {
    path: '/register',
    name: 'register',
    component: register
  },
  {
    path: '/dashborad',
    name: 'dashborad',
    component: dashborad,
    children: [
      {path: '/', redirect: survey, component: survey},
      {path: 'survey', name: 'survey', component: survey},
      {path: 'monitor', name: 'monitor', component: monitor},
      {path: 'info', name: 'info', component: info},


    ]
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
