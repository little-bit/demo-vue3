import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
//import { DatePicker } from 'ant-design-vue';
// import ElementUI from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';


import VideoPlayer from 'vue-video-player/src'
import 'vue-video-player/src/custom-theme.css'
import 'video.js/dist/video-js.css'


const app = createApp(App)
app.config.productionTip = false;
app.use(Antd)
// app.use(ElementUI)
app.use(VideoPlayer)
app.use(store).use(router).mount('#app')
